module gitlab.ethz.ch/lmoeller/apis-go

go 1.15

require (
	github.com/golang/protobuf v1.5.2
	github.com/mwitkow/go-proto-validators v0.3.2
	google.golang.org/genproto v0.0.0-20210429181445-86c259c2b4ab
	google.golang.org/grpc v1.37.0
	google.golang.org/protobuf v1.26.0
)
