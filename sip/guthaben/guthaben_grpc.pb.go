// Code generated by protoc-gen-go-grpc. DO NOT EDIT.

package guthaben

import (
	context "context"
	empty "github.com/golang/protobuf/ptypes/empty"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion7

// GuthabenClient is the client API for Guthaben service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type GuthabenClient interface {
	// ListTransactions will list all the transaction of the user specified
	ListTransactions(ctx context.Context, in *ListTransactionsRequest, opts ...grpc.CallOption) (*ListTransactionsResponse, error)
	// GetTransaction gets the transaction matching the transaction ID
	GetTransaction(ctx context.Context, in *GetTransactionRequest, opts ...grpc.CallOption) (*Transaction, error)
	// CreateTransaction will create an unconfirmed transaction
	CreateTransaction(ctx context.Context, in *CreateTransactionRequest, opts ...grpc.CallOption) (*Transaction, error)
	// ConfirmTransaction will confirm a transaction if applicable to the type of transaction
	ConfirmTransaction(ctx context.Context, in *ConfirmTransactionRequest, opts ...grpc.CallOption) (*Transaction, error)
	// ListAccounts will list all accounts with their balance.
	ListAccounts(ctx context.Context, in *ListAccountsRequest, opts ...grpc.CallOption) (*ListAccountsResponse, error)
	// GetAccount will get the user account. Particularly, its balance
	GetAccount(ctx context.Context, in *GetAccountRequest, opts ...grpc.CallOption) (*Account, error)
	// CreateAccount will create an account for the sub specified.
	CreateAccount(ctx context.Context, in *CreateAccountRequest, opts ...grpc.CallOption) (*Account, error)
	// CheckLiveliness allows to check the availability of Guthaben
	CheckLiveliness(ctx context.Context, in *empty.Empty, opts ...grpc.CallOption) (*empty.Empty, error)
}

type guthabenClient struct {
	cc grpc.ClientConnInterface
}

func NewGuthabenClient(cc grpc.ClientConnInterface) GuthabenClient {
	return &guthabenClient{cc}
}

func (c *guthabenClient) ListTransactions(ctx context.Context, in *ListTransactionsRequest, opts ...grpc.CallOption) (*ListTransactionsResponse, error) {
	out := new(ListTransactionsResponse)
	err := c.cc.Invoke(ctx, "/vseth.sip.guthaben.Guthaben/ListTransactions", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *guthabenClient) GetTransaction(ctx context.Context, in *GetTransactionRequest, opts ...grpc.CallOption) (*Transaction, error) {
	out := new(Transaction)
	err := c.cc.Invoke(ctx, "/vseth.sip.guthaben.Guthaben/GetTransaction", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *guthabenClient) CreateTransaction(ctx context.Context, in *CreateTransactionRequest, opts ...grpc.CallOption) (*Transaction, error) {
	out := new(Transaction)
	err := c.cc.Invoke(ctx, "/vseth.sip.guthaben.Guthaben/CreateTransaction", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *guthabenClient) ConfirmTransaction(ctx context.Context, in *ConfirmTransactionRequest, opts ...grpc.CallOption) (*Transaction, error) {
	out := new(Transaction)
	err := c.cc.Invoke(ctx, "/vseth.sip.guthaben.Guthaben/ConfirmTransaction", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *guthabenClient) ListAccounts(ctx context.Context, in *ListAccountsRequest, opts ...grpc.CallOption) (*ListAccountsResponse, error) {
	out := new(ListAccountsResponse)
	err := c.cc.Invoke(ctx, "/vseth.sip.guthaben.Guthaben/ListAccounts", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *guthabenClient) GetAccount(ctx context.Context, in *GetAccountRequest, opts ...grpc.CallOption) (*Account, error) {
	out := new(Account)
	err := c.cc.Invoke(ctx, "/vseth.sip.guthaben.Guthaben/GetAccount", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *guthabenClient) CreateAccount(ctx context.Context, in *CreateAccountRequest, opts ...grpc.CallOption) (*Account, error) {
	out := new(Account)
	err := c.cc.Invoke(ctx, "/vseth.sip.guthaben.Guthaben/CreateAccount", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *guthabenClient) CheckLiveliness(ctx context.Context, in *empty.Empty, opts ...grpc.CallOption) (*empty.Empty, error) {
	out := new(empty.Empty)
	err := c.cc.Invoke(ctx, "/vseth.sip.guthaben.Guthaben/CheckLiveliness", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// GuthabenServer is the server API for Guthaben service.
// All implementations must embed UnimplementedGuthabenServer
// for forward compatibility
type GuthabenServer interface {
	// ListTransactions will list all the transaction of the user specified
	ListTransactions(context.Context, *ListTransactionsRequest) (*ListTransactionsResponse, error)
	// GetTransaction gets the transaction matching the transaction ID
	GetTransaction(context.Context, *GetTransactionRequest) (*Transaction, error)
	// CreateTransaction will create an unconfirmed transaction
	CreateTransaction(context.Context, *CreateTransactionRequest) (*Transaction, error)
	// ConfirmTransaction will confirm a transaction if applicable to the type of transaction
	ConfirmTransaction(context.Context, *ConfirmTransactionRequest) (*Transaction, error)
	// ListAccounts will list all accounts with their balance.
	ListAccounts(context.Context, *ListAccountsRequest) (*ListAccountsResponse, error)
	// GetAccount will get the user account. Particularly, its balance
	GetAccount(context.Context, *GetAccountRequest) (*Account, error)
	// CreateAccount will create an account for the sub specified.
	CreateAccount(context.Context, *CreateAccountRequest) (*Account, error)
	// CheckLiveliness allows to check the availability of Guthaben
	CheckLiveliness(context.Context, *empty.Empty) (*empty.Empty, error)
	mustEmbedUnimplementedGuthabenServer()
}

// UnimplementedGuthabenServer must be embedded to have forward compatible implementations.
type UnimplementedGuthabenServer struct {
}

func (UnimplementedGuthabenServer) ListTransactions(context.Context, *ListTransactionsRequest) (*ListTransactionsResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ListTransactions not implemented")
}
func (UnimplementedGuthabenServer) GetTransaction(context.Context, *GetTransactionRequest) (*Transaction, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetTransaction not implemented")
}
func (UnimplementedGuthabenServer) CreateTransaction(context.Context, *CreateTransactionRequest) (*Transaction, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CreateTransaction not implemented")
}
func (UnimplementedGuthabenServer) ConfirmTransaction(context.Context, *ConfirmTransactionRequest) (*Transaction, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ConfirmTransaction not implemented")
}
func (UnimplementedGuthabenServer) ListAccounts(context.Context, *ListAccountsRequest) (*ListAccountsResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ListAccounts not implemented")
}
func (UnimplementedGuthabenServer) GetAccount(context.Context, *GetAccountRequest) (*Account, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetAccount not implemented")
}
func (UnimplementedGuthabenServer) CreateAccount(context.Context, *CreateAccountRequest) (*Account, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CreateAccount not implemented")
}
func (UnimplementedGuthabenServer) CheckLiveliness(context.Context, *empty.Empty) (*empty.Empty, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CheckLiveliness not implemented")
}
func (UnimplementedGuthabenServer) mustEmbedUnimplementedGuthabenServer() {}

// UnsafeGuthabenServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to GuthabenServer will
// result in compilation errors.
type UnsafeGuthabenServer interface {
	mustEmbedUnimplementedGuthabenServer()
}

func RegisterGuthabenServer(s grpc.ServiceRegistrar, srv GuthabenServer) {
	s.RegisterService(&_Guthaben_serviceDesc, srv)
}

func _Guthaben_ListTransactions_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ListTransactionsRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(GuthabenServer).ListTransactions(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/vseth.sip.guthaben.Guthaben/ListTransactions",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(GuthabenServer).ListTransactions(ctx, req.(*ListTransactionsRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Guthaben_GetTransaction_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetTransactionRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(GuthabenServer).GetTransaction(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/vseth.sip.guthaben.Guthaben/GetTransaction",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(GuthabenServer).GetTransaction(ctx, req.(*GetTransactionRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Guthaben_CreateTransaction_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CreateTransactionRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(GuthabenServer).CreateTransaction(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/vseth.sip.guthaben.Guthaben/CreateTransaction",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(GuthabenServer).CreateTransaction(ctx, req.(*CreateTransactionRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Guthaben_ConfirmTransaction_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ConfirmTransactionRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(GuthabenServer).ConfirmTransaction(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/vseth.sip.guthaben.Guthaben/ConfirmTransaction",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(GuthabenServer).ConfirmTransaction(ctx, req.(*ConfirmTransactionRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Guthaben_ListAccounts_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ListAccountsRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(GuthabenServer).ListAccounts(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/vseth.sip.guthaben.Guthaben/ListAccounts",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(GuthabenServer).ListAccounts(ctx, req.(*ListAccountsRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Guthaben_GetAccount_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetAccountRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(GuthabenServer).GetAccount(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/vseth.sip.guthaben.Guthaben/GetAccount",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(GuthabenServer).GetAccount(ctx, req.(*GetAccountRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Guthaben_CreateAccount_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CreateAccountRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(GuthabenServer).CreateAccount(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/vseth.sip.guthaben.Guthaben/CreateAccount",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(GuthabenServer).CreateAccount(ctx, req.(*CreateAccountRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Guthaben_CheckLiveliness_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(empty.Empty)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(GuthabenServer).CheckLiveliness(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/vseth.sip.guthaben.Guthaben/CheckLiveliness",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(GuthabenServer).CheckLiveliness(ctx, req.(*empty.Empty))
	}
	return interceptor(ctx, in, info, handler)
}

var _Guthaben_serviceDesc = grpc.ServiceDesc{
	ServiceName: "vseth.sip.guthaben.Guthaben",
	HandlerType: (*GuthabenServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "ListTransactions",
			Handler:    _Guthaben_ListTransactions_Handler,
		},
		{
			MethodName: "GetTransaction",
			Handler:    _Guthaben_GetTransaction_Handler,
		},
		{
			MethodName: "CreateTransaction",
			Handler:    _Guthaben_CreateTransaction_Handler,
		},
		{
			MethodName: "ConfirmTransaction",
			Handler:    _Guthaben_ConfirmTransaction_Handler,
		},
		{
			MethodName: "ListAccounts",
			Handler:    _Guthaben_ListAccounts_Handler,
		},
		{
			MethodName: "GetAccount",
			Handler:    _Guthaben_GetAccount_Handler,
		},
		{
			MethodName: "CreateAccount",
			Handler:    _Guthaben_CreateAccount_Handler,
		},
		{
			MethodName: "CheckLiveliness",
			Handler:    _Guthaben_CheckLiveliness_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "vseth/sip/guthaben/guthaben.proto",
}
